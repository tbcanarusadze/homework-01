package com.example.numbergenerator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        generatorBtn.setOnClickListener {
            randomNumbers()
            val number = randomNumbers()
                Toast.makeText(this, if (number){"ეს რიცხვი არის ლუწი"
                } else{"ეს რიცხვი არის კენტი"}, Toast.LENGTH_SHORT).show()
        }
    }

    private fun randomNumbers():Boolean{
        val number = (1..101).random()
        d("number", number.toString())
        numberTextView.text = number.toString()
        return number %2 ==0

    }
}
